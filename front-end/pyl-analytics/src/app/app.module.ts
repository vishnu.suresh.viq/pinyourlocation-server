import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import {CheckboxModule} from 'primeng/checkbox';
import {TableModule} from 'primeng/table';
import {TabViewModule} from 'primeng/tabview';
import {CalendarModule} from 'primeng/calendar';
import {ChartModule} from 'primeng/chart';
import {ListboxModule} from 'primeng/listbox';

import { FormsModule } from '@angular/forms';
import { DescriptionComponent } from './description/description.component';
import { UsersComponent } from './users/users.component';
import { TimeframeComponent } from './timeframe/timeframe.component';
import { YearselectorComponent } from './utilities/yearselector/yearselector.component';
import { UserselectComponent } from './utilities/userselect/userselect.component';


@NgModule({
  declarations: [
    AppComponent,
    DescriptionComponent,
    UsersComponent,
    TimeframeComponent,
    YearselectorComponent,
    UserselectComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    CheckboxModule,
    TableModule,
    TabViewModule,
    CalendarModule,
    ChartModule,
    ListboxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
