import { Component, OnInit } from '@angular/core';

import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css']
})
export class DescriptionComponent implements OnInit {

  constructor() { }
  
  ignored_words=["will","be","of", "with", "at", "from", "into", "during", "including", "until", "against", "among", "throughout", "despite", "towards", "upon", "concerning", "to", "in", "for", "on", "by", "about", "like", "through", "over", "before", "between", "after", "since", "without", "under", "within", "along", "following", "across", "behind", "beyond", "plus", "except", "but", "up", "out", "around", "down", "off", "above", "near"]
  selectedWord;
  ngOnInit(){
    this.most_frequent_words = this.get_most_frequent_words();
  }
  onChangeYear(selectedYears:string[]){
    this.most_frequent_words=this.get_most_frequent_words(selectedYears);
  }
  most_frequent_words;
  get_most_frequent_words(selectedYears?:string[]){
    let words_freq={
      home:{},
      office:{},
      leave:{}
    };
    let pyl_filtered=window["pyl_analytics"].pinned_locations.filter((l)=>{return l.description!==""});
    let usershash=window["pyl_analytics"].usershash;
    pyl_filtered.forEach(pyl => {
      if(_.isUndefined(selectedYears)||_.includes(selectedYears,moment(pyl.date, 'YYYY/MM/DD').year()+"")){
        let loc:string = pyl.location; 
        let description:string = pyl.description;
        let regexstr=`\\b${this.ignored_words.join("\\b|\\b")}\\b`;
        description.toLowerCase().split(/\s+/).forEach((word)=>{
          if(words_freq[loc][word]==undefined){
            words_freq[loc][word]={
              freq:0,
              orig:[]
            };
          }
          words_freq[loc][word].freq++;
          words_freq[loc][word].orig.push({
            user:usershash[pyl.user_id],
            description:description,
            date:pyl.date
          })
        });
      }
    });
    return [{
      location:"Office",
      words:this.hashtoarr(words_freq["office"])
    },{
      location:"Leave",
      words:this.hashtoarr(words_freq["leave"]),
    },{
      location:"Home",
      words:this.hashtoarr(words_freq["home"])
    }]
  }

  hashtoarr(words_freq){
    return _.reverse(_.sortBy(_.compact(_.map(words_freq,(value,key)=>{
        return {
          word:key,
          frequency:value.freq,
          original:value.orig
        };
    })),"frequency"));
  }
}
