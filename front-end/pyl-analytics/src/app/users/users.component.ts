import { Component, OnInit ,Input} from '@angular/core';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  @Input() usershash;
  @Input() pinned_locations:{
    date:string,
    description:string,
    location:string,
    user_id:number
  }[];
  fromDate:Date;
  toDate:Date;
  constructor() { }

  ngOnInit() {
    this.summary=this.getsummary();
  }
  summary;
  onChangeDate(){
    this.summary=this.getsummary();
  }
  getsummary(){
    let users_info={

    };
    let fromDate=this.fromDate!==undefined?moment(this.fromDate):moment("2015-01-01");
    let toDate=this.toDate!==undefined?moment(this.toDate):moment();
    _.each(this.pinned_locations,(pyl_entry)=>{
      if(moment(pyl_entry.date,'YYYY/MM/DD').isBetween(fromDate,toDate,"day","[]")){
        let user_info:{
          leave:number,home:number,office:number,user_id:number,name:string,startdate:moment.Moment
        };
        if(users_info[pyl_entry.user_id]===undefined){
          user_info={
            leave:0,
            home:0,
            office:0,
            user_id:-1,
            name:"unknown",
            startdate:moment()
          };
          users_info[pyl_entry.user_id]=user_info;
        }else{
          user_info=users_info[pyl_entry.user_id];
        }
        user_info.startdate = moment.min(user_info.startdate,moment(pyl_entry.date,'YYYY/MM/DD'));
        user_info[pyl_entry.location]++;
      }
    });
    return _.map(users_info,(info,id)=>{
      info.user_id=id;
      info.name = this.usershash[id];
      info.leavespermonth = this.calculateRate(info.leave,info.startdate,toDate);
      info.officespermonth = this.calculateRate(info.office,info.startdate,toDate);
      info.homespermonth = this.calculateRate(info.home,info.startdate,toDate);
      return info;
    });

  }

  calculateRate(num,fromDate,toDate){
    return _.round(num/(toDate.diff(fromDate,"days")+1)*30,2)
  }
}
