import { Component, OnInit, Output, EventEmitter} from '@angular/core';

import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-yearselector',
  templateUrl: './yearselector.component.html',
  styleUrls: ['./yearselector.component.css']
})
export class YearselectorComponent implements OnInit {
  selectedYears:string[];
  years:string[];
  @Output() onChange:EventEmitter<string[]>= new EventEmitter<string[]>();
  onChangeYear(){
    this.onChange.emit(this.selectedYears);
  }
  constructor() { }

  ngOnInit() {
    this.years=_.range(2016,moment().year()+1).map(year=>year+"");
    this.selectedYears = _.clone(this.years);
  }

}
