import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YearselectorComponent } from './yearselector.component';

describe('YearselectorComponent', () => {
  let component: YearselectorComponent;
  let fixture: ComponentFixture<YearselectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YearselectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YearselectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
