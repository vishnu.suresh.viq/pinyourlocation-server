import { Component, OnInit ,Input,EventEmitter,Output} from '@angular/core';

@Component({
  selector: 'app-userselect',
  templateUrl: './userselect.component.html',
  styleUrls: ['./userselect.component.css']
})
export class UserselectComponent implements OnInit {
  @Input() users;
  selectedUsers
  @Output() onChange:EventEmitter<string[]>= new EventEmitter<string[]>();
  constructor() { }

  ngOnInit() {
    this.selectedUsers=this.users;
  }
  onChangeSelected(){
    this.onChange.emit(this.selectedUsers);
  }
}
