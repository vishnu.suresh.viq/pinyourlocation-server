import { Component, OnInit, Input} from '@angular/core';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-timeframe',
  templateUrl: './timeframe.component.html',
  styleUrls: ['./timeframe.component.css']
})
export class TimeframeComponent implements OnInit {
  @Input() users;
  @Input() usershash;
  @Input() pinned_locations:{
    date:string,
    description:string,
    location:string,
    user_id:number
  }[];

  month_wise_summary;
  constructor() { }

  ngOnInit() {
    this.month_wise_summary=this.get_month_wise_summary(this.selectedYears,this.selectedUsers);
    
  }
  createData(leaves:number[],offices:number[],homes:number[]){
    return {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December'],
        datasets: [
            {
                label: 'Leaves',
                backgroundColor: '#ac2925',
                borderColor: '#ac2925',
                data: _.clone(leaves)
            },
            {
                label: 'Office',
                backgroundColor: '#5cb85c',
                borderColor: '#5cb85c',
                data: _.clone(offices)
            },
            {
                label: 'Home',
                backgroundColor: '#f0ad4e',
                borderColor: '#f0ad4e',
                data: _.clone(homes)
            }
        ]
    };
  }
  get_month_wise_summary(selectedYears?:string[],selectedUsers?){
    let stat={
      leave:_.times(12,_.constant(0)),
      office:_.times(12,_.constant(0)),
      home:_.times(12,_.constant(0))
    }
    _.each(this.pinned_locations,(pyl)=>{
      let date:moment.Moment=moment(pyl.date, 'YYYY/MM/DD');
      if((_.isUndefined(selectedUsers)||_.some(selectedUsers,['id', pyl.user_id]))&&(_.isUndefined(selectedYears)||_.includes(selectedYears,date.year()+""))){
        stat[pyl.location][date.month()]++;
      }
    });
    return this.createData(stat.leave,stat.office,stat.home);
  }
  selectedYears:string[];
  onChangeYear(selectedYears:string[]){
    this.selectedYears=selectedYears;
    this.month_wise_summary=this.get_month_wise_summary(this.selectedYears,this.selectedUsers);
  }
  selectedUsers;
  onChangeUsers(selectedUsers){
    this.selectedUsers=selectedUsers;
    this.month_wise_summary=this.get_month_wise_summary(this.selectedYears,this.selectedUsers);
  }
}
