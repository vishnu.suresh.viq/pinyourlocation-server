import { Component , OnInit, OnChanges} from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pyl-analytics';

  pinned_locations;
  usershash;
  users;

  tab_index=0;

  ngOnInit(){
    this.pinned_locations=window["pyl_analytics"].pinned_locations;
    this.usershash=window["pyl_analytics"].usershash;
    this.users=window["pyl_analytics"].users;
  }
}
