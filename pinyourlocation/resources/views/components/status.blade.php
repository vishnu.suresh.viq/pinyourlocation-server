<div class="panel panel-default">
    <div class="panel-heading">Where will you be working today?</div>
    <div class="panel-body">
        <form action="/location{{is_numeric($location->id)?'/'.$location->id:''}}" method="post">
            <div class="row">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                @if(is_numeric($location->id))
                    <input type="hidden" name="_method" value="PATCH">
                @endif
                <div class="btn-group btn-group-justified btn-group-lg col-sm-12" role="group">
                    <div class="btn-group" role="group">
                        <button type='submit' name="location" value="home" class="btn btn-{{$location->location==='home'?'warning':'default'}}"><span class="glyphicon glyphicon-home"></span> Home</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type='submit' name="location" value="office" class="btn btn-{{$location->location==='office'?'success':'default'}}"><span class="glyphicon glyphicon-briefcase"></span> Office</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type='submit' name="location" value="leave" class="btn btn-{{$location->location==='leave'?'danger':'default'}}"><span class="glyphicon glyphicon-off"></span> Leave</button>
                    </div>
                </div>
            </div>
            <br />
            <div class="row">
                <div class='col-sm-12'>
                @if($location->location)
                <div class="input-group">
                    <input type="text" name="description" class="form-control" placeholder="Leave a comment if you like" value='{{$location->description}}'>
                    <span class="input-group-btn">
                        <button title="Submit Comment" type='submit' name="location" value="{{$location->location}}" class="btn btn-default"><span class="glyphicon glyphicon-comment"></span></button>
                    </span>
                </div>
                @else
                    <input type="text" name="description" class="form-control" placeholder="Leave a comment if you like">
                @endif
                </div>
            </div>
        </form>
    </div>
</div>