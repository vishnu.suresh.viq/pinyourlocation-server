@extends('vendor.entrust-gui.container')

@section('heading', 'Corrections')

@section('entrust-content')
    <div class="row">
        <div class="col-md-9">
            <div class="canvas">
            </div>
            <script>
            var h = maquette.h;
            var projector = maquette.createProjector();
            var searchterm="";
            var statusfilter="all"
            var users=[
            @foreach ($users as $index=>$user)
                @unless ($index===0)
                ,
                @endunless
                {
                    name:'{{$user["name"]}}',
                    id:'{{$user["id"]}}',
                    status:'{{$user["status"]}}'
                }
            @endforeach
            ];
            var stats={
                office:0,
                unmarked:0,
                leave:0,
                home:0,
                all:users.length
            };
            users.forEach(function(user){
                switch (user.status) {
                    case 'office':
                        stats.office++;
                        break;
                    case 'unmarked':
                        stats.unmarked++;
                        break;
                    case 'leave':
                        stats.leave++;
                        break;
                    case 'home':
                        stats.home++;
                        break;
                }
            });
            function onsearch(evt){
                searchterm = evt.target.value||"";
            }
            function filterall(){
                statusfilter="all";
            }
            function filteroffice(){
                statusfilter="office";
            }
            function filterhome(){
                statusfilter="home";
            }
            function filterleave(){
                statusfilter="leave";
            }
            function filterunmarked(){
                statusfilter="unmarked";
            }
            function renderMaquette() {
                return h('div.form-group', [
                    h('input.form-control',{
                        placeholder:"Search",
                        onkeyup:onsearch,
                        value:searchterm
                    }),
                    h('br'),
                    h('form',[
                        h("label.radio-inline",h("input",{
                            type:"radio",
                            name:"status",
                            onclick:filterall
                        }),"All("+stats.all+")"),
                        h("label.radio-inline",h("input",{
                            type:"radio",
                            name:"status",
                            onclick:filteroffice
                        }),"Office("+stats.office+")"),
                        h("label.radio-inline",h("input",{
                            type:"radio",
                            name:"status",
                            onclick:filterhome
                        }),"Home("+stats.home+")"),
                        h("label.radio-inline",h("input",{
                            type:"radio",
                            name:"status",
                            onclick:filterleave
                        }),"Leave("+stats.leave+")"),
                        h("label.radio-inline",h("input",{
                            type:"radio",
                            name:"status",
                            onclick:filterunmarked
                        }),"Unmarked("+stats.unmarked+")"),
                    ]),
                    h('table.table', [
                        h('thead',h("tr",h("th","User"),h("th","Status"))),
                        h('tbody',users.filter(function(user){
                            return (
                                (user.name.toLowerCase().indexOf(searchterm.toLowerCase())>-1)
                                &&
                                ((user.status===statusfilter)||(statusfilter=="all"))
                            );
                        }).map(function(user){
                            return h("tr",{key: user.id},
                                h("td",user.name),
                                h("td",
                                    h("form",{
                                        "method":"post",
                                    },[
                                        h("input",{
                                            type:"hidden",
                                            name:"_token",
                                            value:"{{{ csrf_token() }}}"
                                        }),
                                        h("input",{
                                            type:"hidden",
                                            name:"user_id",
                                            value:user.id
                                        }),
                                        h("input",{
                                            type:"hidden",
                                            name:"date",
                                            value:"{{$date->format('d-m-Y')}}"
                                        }),
                                        h("div",{
                                             class:"btn-group",
                                             role:"group"
                                        },[
                                            h("button",{
                                                type:"submit",
                                                name:"location",
                                                value:"home",
                                                class:"btn btn-"+(user.status==="home"?"warning":"default")
                                            },h("span",{
                                                class:"glyphicon glyphicon-home"
                                            }), " Home"),

                                            h("button",{
                                                type:"submit",
                                                name:"location",
                                                value:"office",
                                                class:"btn btn-"+(user.status==="office"?"success":"default")
                                            },h("span",{
                                                class:"glyphicon glyphicon-briefcase"
                                            }), " Office"),

                                            h("button",{
                                                type:"submit",
                                                name:"location",
                                                value:"leave",
                                                class:"btn btn-"+(user.status==="leave"?"danger":"default")
                                            },h("span",{
                                                class:"glyphicon glyphicon-off"
                                            })," Leave")
                                        ]),
                                    ])
                                ),
                                h("td",user.status)
                            );
                        }))
                    ])
                ]);
            }

            // Initializes the projector 
            document.addEventListener('DOMContentLoaded', function () {
                projector.append($(".canvas").get(0), renderMaquette);
            });
            </script>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div id="datepicker" data-date="{{$date->format('d-m-Y')}}"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#datepicker').datepicker({
            format:"dd-mm-yyyy"
        });
        $('#datepicker').on("changeDate", function() {
            window.location.href = $('#datepicker').datepicker('getFormattedDate');
        });
    </script>
    <style>
        #datepicker table{
            margin:auto;
        }
    </style>
    
@endsection
