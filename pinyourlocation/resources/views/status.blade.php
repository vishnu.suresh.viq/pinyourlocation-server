@extends('layouts.app')

@section('content')
<div class="p-modal">

        <div class="container">
            <div class='status-component'>
                @include('components.status',[
                    "location"=>$location,
                    "user"=>$user
                ])
            </div>
        </div>
</div>
<style>
.p-modal{
    position:fixed;
    background:black;
    top:0px;
    bottom:0px;
    left:0px;
    right:0px;
    z-index: 999;
    background-color: rgba(0,0,0,.85);
}
.status-component{
    position:absolute;
    top: 50%;
    left:50%;
    margin-top:-80px;
    margin-left:-150px;
    width:300px;
}
</style>
@endsection