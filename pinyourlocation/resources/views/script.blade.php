var base_url="{{url('/')}}";
var installdependencies=(new Promise(function(resolve){
    var npm = require('npm');
    npm.load(function(err) {
        npm.commands.install(['network','node-arp','open'], function(er, data) {
            resolve();
        });
    });
}));

var isinoffice=installdependencies.then(function(){
    return (new Promise(function(resolve,reject){
        var network = require('network');
        var arp = require('node-arp');
        network.get_gateway_ip(function(err, ip) {
            if(!err){
                arp.getMAC(ip, function(err, mac) {
                    if (!err) {
                        var request = require('request');
                        request.post({
                            url:`${base_url}/api/v1/gateway`,
                            form: {
                                token:'{{$token}}',
                                macaddress:mac
                            }
                        });
                        var gateways=[];
                        @foreach ($gateways as $gateway)
                            gateways.push("{{$gateway->macaddress}}");
                        @endforeach
                        if(gateways.some(function(office_mac){
                            if(office_mac===mac){
                                return true;
                            }
                        })){
                            resolve(true);
                        }
                        else{
                            resolve(false);
                        }
                    }
                    else{
                        reject(err);
                    }
                });
            }
            else{
                reject(err);
            }
        });
    }));
},function(err){
    var request = require('request');
    request.post(`${base_url}/api/v1/logs`).form({
        key:'client-error',
        value:JSON.stringify(err)
    });
});

isinoffice.then(function(office){
    var request = require('request');
    if(office){
        request.post({
            url:`${base_url}/api/v1/setoffice`,
            form: {
                token:'{{$token}}'
            }
        });
    }
    else{
        @if ($should_show_popup)
            var open = require('open');
            open(`${base_url}/authenticatebytoken/{{$token}}`);
        @endif
    }
})
