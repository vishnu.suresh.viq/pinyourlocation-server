@extends('layouts.app')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
    <table class="table">
        <thead>
            <tr>
                <th>User History</th><th class='text-center'>Last 2 weeks</th><th class='text-center'>Today</th>
            </tr>
        </thead>
        <tbody id="user_history">
        
            <tr>
                <td>
                Loading
                </td>
                <td class="text-center">
                
                </td>
            </tr>
        </tbody>
    </table>
    <style>
        .location{
            display:inline-block;
            width:30px;
            height:30px;
            margin:2px;
            padding-top:5px;
            font-size:12px;
            color:white;
            font-weight:bold;
        }
    </style>
    <script>
    $(function () {
        // <div class='location locationtype' title="date" data-toggle="tooltip"></div>
        var num=14;
        var history=new Promise(function(resolve,reject){
            $.getJSON("/api/v1/alluserhistory/"+num).done(function(users) {
                resolve(users);
            })
            .fail(function() {
                reject();
            });
        });
        var holidays=new Promise(function(resolve,reject){
        $.getJSON("/api/v1/holidays").done(function(holidays) {
                resolve(holidays);
            })
            .fail(function() {
                reject();
            });
        });
        Promise.all([history,holidays]).then(function(arr){
            var users=arr[0];
            var holidays=arr[1];
            var tbody=$("#user_history").empty();
            users.forEach(function(user){
                var row="<tr><td><a href='user/"+user.id+"'>"+user.name+"</a></td><td class='text-center'>";
                var t_num=num;
                while(t_num){
                    var day=moment().subtract(t_num-1,"days");
                    var location="unmarked";
                    if(!(day.day()%6)){
                        location="weekend";
                    }else{
                        location=(user.pinned_locations.find(function(location){
                            return (location.date===day.format("YYYY-MM-DD"));
                        })||{location:"unmarked"}).location
                    }
                    if(t_num===1){
                        row+= "</td><td class='text-center'>";
                    }
                    row+= "<div class='location "+location+"' title="+day.format("DD-MM-YYYY")+" data-toggle='tooltip'>"+day.format("DD")+"</div>";
                    t_num--;
                }
                row+="</td></tr>";
                tbody.append($(row));
            });
            $('[data-toggle="tooltip"]').tooltip()
        });
    })
    </script>
    </div>
</div>
@endsection
