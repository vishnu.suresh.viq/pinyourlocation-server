@extends('vendor.entrust-gui.container')

@section('heading', 'Gateway')

@section('entrust-content')
<table class='table'>
    <thead>
        <tr>
            <th>Gateway</th>
            <th>Count</th>
            <th>Office</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($logs as $log)
        <tr>
            <td>{{ $log->gateway }}</td>
            <td>{{ $log->count }}</td>
            <td>
                <div class="ui toggle checkbox">
                    <input class="toggleoffice" data-macaddress="{{$log->gateway}}" type="checkbox" name="newsletter" {{$log->office?"checked=checked":""}}>
                    <label></label>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<script>
$(".toggleoffice").click(function(){
    var macaddress=$(this).data("macaddress");
    $.post("gateway/"+macaddress+"/toggle");
});
</script>
@endsection
