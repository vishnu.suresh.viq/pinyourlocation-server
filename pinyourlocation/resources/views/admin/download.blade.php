@extends('vendor.entrust-gui.container')

@section('heading', 'Download')

@section('entrust-content')
<button onclick="download_history()" class="btn btn-primary">Download History</button>
<script>
    var pyl_analytics = {};
    (function(namespace){
        var users = {!!json_encode($users)!!}

        var usershash = {};
        users.forEach((user)=>{
            usershash[user.id]=user.name;
        });
        var pinned_locations = [
            @foreach ($pinnedlocations as $pinnedlocation) {user_id:{{$pinnedlocation->user_id}},date:"{{$pinnedlocation->date}}",location:"{{$pinnedlocation->location}}",description:`{{$pinnedlocation->description}}`},
            @endforeach
        ];
        namespace.users=users;
        namespace.pinned_locations=pinned_locations;
        namespace.usershash=usershash;
    })(pyl_analytics);
    csv = pyl_analytics.pinned_locations.map((pl)=>{return [pyl_analytics.usershash[pl.user_id],pl.date,pl.location,pl.description]})
    csv.unshift(["name","date","location","description"])
    pyl_analytics.pinned_locations.map((pl)=>{return [pyl_analytics.usershash[pl.user_id],pl.date,pl.location,pl.description]})
    function download_history(){
        exportToCsv("location history.csv", csv)
    }
    
    function exportToCsv(filename, rows) {
        var processRow = function (row) {
            var finalVal = '';
            for (var j = 0; j < row.length; j++) {
                var innerValue = row[j] === null ? '' : row[j].toString();
                if (row[j] instanceof Date) {
                    innerValue = row[j].toLocaleString();
                };
                var result = innerValue.replace(/"/g, '""');
                if (result.search(/("|,|\n)/g) >= 0)
                    result = '"' + result + '"';
                if (j > 0)
                    finalVal += ',';
                finalVal += result;
            }
            return finalVal + '\n';
        };

        var csvFile = '';
        for (var i = 0; i < rows.length; i++) {
            csvFile += processRow(rows[i]);
        }

        var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            var link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }
</script>

@endsection
