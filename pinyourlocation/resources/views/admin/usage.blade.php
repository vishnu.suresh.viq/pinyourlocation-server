@extends('vendor.entrust-gui.container')

@section('heading', 'Usage Statistics')

@section('entrust-content')
<p>This page shows the last time user's client contacted the server. <button class="btn btn-success" id="btnExport" onclick="fnExcelReport();"> <i class="fa fa-file-excel-o" aria-hidden="true"></i></button> </p>
<iframe id="txtArea1" style="display:none"></iframe>

<div class="canvas">
</div>
<script>
    function fnExcelReport()
    {
        var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j=0;
        tab = document.getElementById('usagestattable'); // id of table

        for(j = 0 ; j < tab.rows.length ; j++) 
        {     
            tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text=tab_text+"</table>";
        tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
        tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE "); 

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html","replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus(); 
            sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
        }  
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

        return (sa);
    }

    var h = maquette.h;
    var projector = maquette.createProjector();
    var searchterm="";
    var statusfilter="all"

    function onsearch(evt){
        searchterm = evt.target.value||"";
    }
    function filterall(){
        statusfilter="all";
    }
    function filteroffice(){
        statusfilter="office";
    }
    function filterhome(){
        statusfilter="home";
    }
    function filterleave(){
        statusfilter="leave";
    }
    function filterunmarked(){
        statusfilter="unmarked";
    }
    
    var users=[];
    @foreach ($users as $user)
    users.push({
        id:"{{$user->id}}",
        name:"{{$user->name}}",
        last_script_fetch:"{{$user->last_script_fetch}}",
        email:"{{$user->email}}",
    });
    @endforeach
    users.sort(function(a,b){
        a=a.last_script_fetch===""?moment("1990-01-01","YYYY-MM-DD"):moment(a.last_script_fetch,"YYYY-MM-DD HH-mm-ss");
        b=b.last_script_fetch===""?moment("1990-01-01","YYYY-MM-DD"):moment(b.last_script_fetch,"YYYY-MM-DD HH-mm-ss");
        return a.diff(b);
    });
    function renderMaquette() {
        return h('div.form-group', [
            h('input.form-control',{
                placeholder:"Search",
                onkeyup:onsearch,
                value:searchterm
            }),
            h('br'),
            /*h('form',[
                h("label.radio-inline",h("input",{
                    type:"radio",
                    name:"status",
                    onclick:filterall
                }),"All"),
                h("label.radio-inline",h("input",{
                    type:"radio",
                    name:"status",
                    onclick:filteroffice
                }),"Office"),
                h("label.radio-inline",h("input",{
                    type:"radio",
                    name:"status",
                    onclick:filterhome
                }),"Home"),
                h("label.radio-inline",h("input",{
                    type:"radio",
                    name:"status",
                    onclick:filterleave
                }),"Leave"),
                h("label.radio-inline",h("input",{
                    type:"radio",
                    name:"status",
                    onclick:filterunmarked
                }),"Unmarked"),
            ]),*/
            h('table.table#usagestattable', [
                h('thead',h("tr",h("th","User"),h("th","Email"),h("th","Status"))),
                h('tbody',users.filter(function(user){
                    return (
                        (user.name.toLowerCase().indexOf(searchterm.toLowerCase())>-1)
                        &&
                        ((user.status===statusfilter)||(statusfilter=="all"))
                    );
                }).map(function(user){
                    return h("tr",{key: user.id},
                        h("td",user.name),
                        h("td",user.email),
                        h("td",user.last_script_fetch===""?"Never":moment(user.last_script_fetch,"YYYY-MM-DD HH-mm-ss").fromNow())
                    );
                }))
            ])
        ]);
    }

    // Initializes the projector 
    document.addEventListener('DOMContentLoaded', function () {
        projector.append($(".canvas").get(0), renderMaquette);
    });
</script>
@endsection
