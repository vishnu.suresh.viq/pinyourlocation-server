@extends('vendor.entrust-gui.container')

@section('heading', 'Analytics')

@section('entrust-content')
<script>
    var pyl_analytics = {};
    (function(namespace){
        var users = {!!json_encode($users)!!}

        var usershash = {};
        users.forEach((user)=>{
            usershash[user.id]=user.name;
        });
        var pinned_locations = [
            @foreach ($pinnedlocations as $pinnedlocation) {user_id:{{$pinnedlocation->user_id}},date:"{{$pinnedlocation->date}}",location:"{{$pinnedlocation->location}}",description:`{{$pinnedlocation->description}}`},
            @endforeach
        ];
        namespace.users=users;
        namespace.pinned_locations=pinned_locations;
        namespace.usershash=usershash;
    })(pyl_analytics);
</script>
<app-root></app-root>
<script type="text/javascript" src="/apps/analytics/runtime.js"></script>
<script type="text/javascript" src="/apps/analytics/polyfills.js"></script>
<script type="text/javascript" src="/apps/analytics/styles.js"></script>
<script type="text/javascript" src="/apps/analytics/scripts.js"></script>
<script type="text/javascript" src="/apps/analytics/vendor.js"></script>
<script type="text/javascript" src="/apps/analytics/main.js"></script>

@endsection
