@extends('layouts.app')

@section('content')
<div class="container">
    @if(Entrust::hasRole('verified'))
    <div class="row">
        <div class='col-sm-12'>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('mailsent'))
            <div class="alert alert-success">
                Mail has been sent successfully
            </div>
        @endif
        </div>
    </div>
    <div class="row">
        <div class='col-sm-6 col-lg-4 col-md-5'>
            @include('components.status',[
                "location"=>$location,
                "user"=>$user
            ])
        </div>
        <div class='col-sm-6 col-md-7 col-lg-8'>
            <div class="panel panel-default">
                <div class="panel-heading">
                Do you have any future plans? Or, would you like to request a change in your history?
                </div>
                <div class="panel-body">
                    <form action="locations" method="post">
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    <div class="row">
                        <div class="col-sm-7">
                        <div class="input-daterange input-group input-group-justified" id="datepicker">
                            <input type="text" class="form-control" name="from" placeholder="From"/>
                            <span class="input-group-addon">to</span>
                            <input type="text" class="form-control" name="to" placeholder="To"/>
                        </div>
                        </div>
                        <div class="col-sm-5">
                        <div class="btn-group btn-group-justified btn-group-lg" role="group">
                            <div class="btn-group" role="group">
                                <button type='submit' name="location" value="home" class="btn btn-default"><span class="glyphicon glyphicon-home"></span> Home</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type='submit' name="location" value="leave" class="btn btn-default"><span class="glyphicon glyphicon-off"></span> Leave</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="submit" name="location" value="office" class="btn btn-default" data-toggle="tooltip" title="Past dates: Office | Future dates: Remove">
                                    <span class="glyphicon glyphicon-briefcase"></span>
                                    /
                                    <span class="glyphicon glyphicon-erase"></span>
                                </button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                        <input type="text" name="description" class="form-control" placeholder="Leave a comment if you like">
                        </div>
                    </div>
                    </form>
                    <script>
                    $('.input-daterange').datepicker({

                    });
                    </script>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class='col-sm-12'>
            <div class="panel panel-default">
                <div class="panel-heading">Your Team</div>
                <div class="panel-body">
                    @forelse ($followings as $user)
                        <?php
                            $location = $user->pinned_locations()->where('date',\Carbon\Carbon::today())->first();
                            if(!$location){
                                $status = "unmarked";
                                $description = "";
                            }else{
                                $status = $location->location;
                                $description = $location->description;
                            }
                        ?>
                        <div class="following" data-toggle="tooltip"
                            @if ($status==='office')
                                title="{{ $user->name }} is working from office today"
                            @elseif ($status==='home')
                                title="{{ $user->name }} is working from home today"
                            @elseif ($status==='leave')
                                title="{{ $user->name }} is on leave today"
                            @else
                                title="We don't have information available about {{ $user->name }}’s working location today"
                            @endif
                        >
                            <table>
                                <tr>
                                    <td class='profilepic'>
                                        {!! Gravatar::image($user->email,'profile',['width' => 35, 'height' => 35]) !!}
                                    </td>
                                    <td class='user'>
                                        <table>
                                            <tr>
                                                <td><span class='name'><strong>{{ $user->name }}</strong></span></td>
                                            </tr>
                                            <tr>
                                                <td><div class='description' title="{{$description}}">{{$description}}</div></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class='status {{$status}}'>
                                        @if ($status==='office')
                                            <span class="glyphicon glyphicon-briefcase"></span>
                                        @elseif ($status==='home')
                                            <span class="glyphicon glyphicon-home"></span>
                                        @elseif ($status==='leave')
                                            <span class="glyphicon glyphicon-off"></span>
                                        @else
                                            <span class="glyphicon glyphicon-question-sign"></span>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                    @empty

                    @endforelse
                    <a class='add_people' href="{{url('user')}}">+Add people</a>
                </div>
                <style>
                    .following{
                        float:left;
                        -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
                        box-shadow: 0 1px 1px rgba(0,0,0,.05);
                        border: 1px solid #ddd;
                        -webkit-border-radius: 3px;
                        -moz-border-radius: 3px;
                        border-radius: 3px;
                        margin:5px;
                        padding:5px;
                    }
                    .following .profilepic img{
                        border-radius: 50%;
                    }
                    .following .status{
                        width:35px;
                        height:35px;
                        -webkit-border-radius: 3px;
                        -moz-border-radius: 3px;
                        border-radius: 3px;
                    }
                    .following .status>span{
                        color:white;
                        margin:10px;
                    }
                    .following .user{
                        padding:0px 5px;
                    }
                    .following .description{
                        font-size: 12px;
                        display: block;
                        height:16px;
                        max-width: 150px;
                        white-space: nowrap;
                        text-overflow: ellipsis;
                        overflow: hidden;
                    }
                    .oneline{
                        
                    }
                    .add_people{
                        display:inline-block;
                        padding:10px;
                    }
                </style>
                <script>
                $(".description").tooltip();
                $(document).ready(function(){
                    $('[data-toggle="tooltip"]').tooltip(); 
                });
                </script>
            </div>
        </div>
    </div>
    <div class="row">
        <div class='col-sm-12'>
            <div class="panel panel-default">
                <div class="panel-heading">Your History</div>
                <div class="panel-body">
                        @include('components.history',['user' => Auth::user()])
                </div>
            </div>
        </div>
    </div>
@else
    <div class="alert alert-danger" role="alert">
        You need to verify your email to use this app. See your inbox.
    </div>
@endif
</div>
@endsection
