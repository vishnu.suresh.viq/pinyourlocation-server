<?php

use Faker\Factory as Faker;
use App\Models\Holiday;
use App\Repositories\HolidayRepository;

trait MakeholidayTrait
{
    /**
     * Create fake instance of holiday and save it in database
     *
     * @param array $holidayFields
     * @return Holiday
     */
    public function makeholiday($holidayFields = [])
    {
        /** @var HolidayRepository $holidayRepo */
        $holidayRepo = App::make(HolidayRepository::class);
        $theme = $this->fakeholidayData($holidayFields);
        return $holidayRepo->create($theme);
    }

    /**
     * Get fake instance of holiday
     *
     * @param array $holidayFields
     * @return Holiday
     */
    public function fakeholiday($holidayFields = [])
    {
        return new Holiday($this->fakeholidayData($holidayFields));
    }

    /**
     * Get fake data of holiday
     *
     * @param array $postFields
     * @return array
     */
    public function fakeholidayData($holidayFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->text,
            'date' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $holidayFields);
    }
}
