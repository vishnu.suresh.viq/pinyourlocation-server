<?php

namespace App\Http\Controllers\API\PinYourLocation;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PinnedLocation;
use Carbon\Carbon;
use App\User;

class LocationController extends Controller
{
    public function store_office_via_api(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
        $user=User::where('token', $request->input('token'))->firstOrFail();
        if($user->pinned_locations()->where('date',Carbon::today())->count()===0){
            $p=new PinnedLocation;
            $p->location = "office";
            $p->description = "";
            $p->date = Carbon::today();
            $user->pinned_locations()->save($p);
            return "TRUE";
        }
        return "FALSE";
    }
    public function all_user_history($days)
    {
        $from=Carbon::today()->subDays($days);
        $to=Carbon::today();
        return User::with(['pinned_locations' => function ($query) use($from,$to) {
            $query->whereBetween('date', [$from,$to]);
        }])->orderBy('name','asc')->get()->makeHidden(['email','updated_at','lastrun','created_at'])->toJSON();
    }
    
    public function sync_essl(Request $request)
    {
        $datestr=$request->input('date');
        $date;
        if($datestr){
            $date=Carbon::createFromFormat('d-m-Y', $datestr)->startOfDay();
        }else{
            $date=Carbon::today()->startOfDay();
        }
        $emails=explode("|",$request->input('emails'));
        $done="Done: ";
        foreach($emails as $email){
            $user=User::where('email', $email)->first();
            if($user&&$user->pinned_locations()->where('date',$date)->count()===0){
                $p=new PinnedLocation;
                $p->location = "office";
                $p->description = "";
                $p->date = $date;
                $user->pinned_locations()->save($p);
                $done=$done.$email;
            }
        }
        return $done;
    }
}
