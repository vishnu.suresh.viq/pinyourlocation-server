<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Gateway;
use App\GatewayLog;
use Carbon\Carbon;

class GatewayController extends Controller
{
    public function log(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'macaddress' => 'required'
        ]);
        $user=User::where('token', $request->input('token'))->firstOrFail();
        $gateway=Gateway::firstOrCreate(['macaddress' => $request->input('macaddress')]);
        $date=Carbon::today();
        
        $returnvalue="TRUE";

        try{
            $log = new GatewayLog;
            $log->gateway()->associate($gateway);
            $log->date=$date;
            $log->user()->associate($user);
            $log->save();
        }
        catch (\Illuminate\Database\QueryException $e){
            $returnvalue="FALSE";
        }
        return $returnvalue;
    }
}
