<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Carbon\Carbon;
use App\Gateway;
use App\GatewayLog;
use Illuminate\Support\Facades\DB;

class GatewayController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }
    /**
     * Show the gateway dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gateways = Gateway::all();
        $logs = DB::select("SELECT macaddress as gateway , IFNULL(count,0) as count , office FROM gateways LEFT JOIN (SELECT gateway_id, COUNT(*) as count FROM gateway_logs WHERE date >= ? GROUP BY gateway_id) t1 on t1.gateway_id = gateways.id ORDER BY office DESC, count DESC",[Carbon::today()->subWeek()]);
        return view('admin.gateway')->with('gateways',$gateways)->with('logs',$logs);
    }
    public function toggle($macaddress){
        $gateway=Gateway::where("macaddress","=",$macaddress)->firstOrFail();
        $gateway->office=!$gateway->office;
        $gateway->save();
        return "TRUE";
    }
}
