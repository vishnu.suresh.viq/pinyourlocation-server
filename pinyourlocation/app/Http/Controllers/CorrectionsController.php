<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Carbon\Carbon;
use App\User;
use App\PinnedLocation;
use DB;
class CorrectionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($datestr)
    {
        $date=Carbon::createFromFormat('d-m-Y', $datestr)->startOfDay();
        DB::enableQueryLog();
        $users=User::with(['pinned_locations' => function ($query) use($date) {
            $query->where('date', $date);
        }])->orderBy('name','asc')->get()->map(function ($user, $key) {
            $location=$user->pinned_locations->first();
            if($location){
                $status=$location->location;
            }else{
                $status="unmarked";
            }
        
            return [
                "id"=>$user->id,
                "name"=>$user->name,
                "home"=>($status==="home")?"warning":"default",
                "office"=>($status==="office")?"success":"default",
                "leave"=>($status==="leave")?"danger":"default",
                "status"=>$status
            ];
        });
        return view('corrections')->with('date',$date)->with('users',$users);
    }
    public function update(Request $request)
    {
        $date=Carbon::createFromFormat('d-m-Y', $request->input('date'))->startOfDay();
        $status = $request->input('location');
        $user_id = $request->input('user_id');
        $user=User::findOrFail($user_id);
        $location=$user->pinned_locations()->where('date',$date)->first();
        if($location){
            $location->location=$status;
            $location->save();
        }else{
            $location=new PinnedLocation;
            $location->location=$status;
            $location->date=$date;
            $user->pinned_locations()->save($location);
        }
        // if($request->input('location')!=="office"){
        //     $this->mailFollowers($request->input('location'),Carbon::today(),false);
        // }
        return back();
    }
}
