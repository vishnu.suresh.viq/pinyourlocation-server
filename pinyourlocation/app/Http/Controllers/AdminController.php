<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\PinnedLocation;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function usage()
    {
        return view('admin.usage')->with('users',User::orderby("name","asc")->get());
    }
    public function analytics()
    {
        return view('admin.analytics',[
            'users' => User::select("id","name")->get(),
            'pinnedlocations' => PinnedLocation::select("date","location","description","user_id")->cursor()
        ]);
    }
    public function download()
    {
        return view('admin.download',[
            'users' => User::select("id","name")->get(),
            'pinnedlocations' => PinnedLocation::select("date","location","description","user_id")->cursor()
        ]);
    }
    
}
