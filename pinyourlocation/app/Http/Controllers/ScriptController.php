<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PinnedLocation;
use App\User;
use App\Gateway;
use App\Models\Holiday;
use Auth;
use Carbon\Carbon;

class ScriptController extends Controller
{
    public function code($token)
    {
        $user=User::where('token', $token)->firstOrFail();
        $user->last_script_fetch=Carbon::now();
        $user->save();
        
        $has_no_location_entry_today=$user->pinned_locations()->where('date',Carbon::today())->count()===0;
        $has_not_popup_today=(!$user->lastrun->isToday());
        $is_time_when_app_should_report=Carbon::now()->between(Carbon::createFromTime(6,0,0),Carbon::createFromTime(21, 0, 0));
        $is_allowed_to_disturb_now=Carbon::now()->between(Carbon::createFromTime(13,0,0),Carbon::createFromTime(23, 59, 59));
        $should_show_popup=$is_allowed_to_disturb_now&&$has_not_popup_today;
        if(
            $is_time_when_app_should_report&&
            $has_no_location_entry_today&&
            Carbon::today()->isWeekday()&&
            (Holiday::where('date',Carbon::today())->count()===0)
        ){
            $gateways = Gateway::where("office",1)->get();
            return view('script',
                array(
                    "token"=>$token,
                    "gateways"=>$gateways,
                    "should_show_popup"=>$should_show_popup
                )
            );
        }else{
            $message="//";
            if(!$is_time_when_app_should_report){
                 $message= $message." Reporting is only enabled from 6AM to 9PM.";
            }
            if(!$has_no_location_entry_today){
                 $message= $message." User already entered location.";
            }
            if(!Carbon::today()->isWeekday()){
                 $message= $message." Today is weekend.";
            }
            if(!(Holiday::where('date',Carbon::today())->count()===0)){
                 $message= $message." Today is holiday.";
            }
            return $message;
        }
    }
}
