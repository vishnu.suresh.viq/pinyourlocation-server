<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/

Route::resource('holidays', 'HolidayAPIController');
Route::post('setoffice', 'PinYourLocation\LocationController@store_office_via_api');
Route::get('alluserhistory/{days}', 'PinYourLocation\LocationController@all_user_history' )->middleware('role:manager');
Route::post('gateway', 'GatewayController@log');
Route::post('sync_essl', 'PinYourLocation\LocationController@sync_essl');

Route::resource('logs', 'LogAPIController');