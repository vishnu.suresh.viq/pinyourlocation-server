<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        "api/v1/setoffice",
        "api/v1/scriptfinish",
        "api/v1/logs",
        "api/v1/gateway",
        "api/v1/sync_essl"
    ];
}
