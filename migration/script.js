var mysql = require('mysql');
var crypto = require('crypto');
var base64url = require('base64url');
var moment = require('moment');
console.log("started");
var source_connection = mysql.createConnection({
    host: '52.11.117.122',
    user: 'devteam',
    password: 'eLKDrBWlQIvDgppiJZ6M',
    database: 'wfh'
});
var destination_connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'toor',
    database: 'homestead'
});
function p(callback){
    return new Promise(function(resolve){
        callback(resolve);
    });
}
source_connection.connect();
destination_connection.connect();
p((r)=>{
    // destination_connection.query(`SET FOREIGN_KEY_CHECKS = 0;TRUNCATE users;TRUNCATE pinned_locations;SET FOREIGN_KEY_CHECKS = 1;`, function (err, result) {
    //     if (err) throw err;
        r();
    // });
}).then(()=>{
    return p((r)=>{
        console.log("migrating users");
        source_connection.query('SELECT * from user', function (err, rows, fields) {
            if (err) throw err;
            users=rows.map(function (r) {
                return [
                    r.firstname + " " + r.lastname,
                    r.email,
                    r.password||"Wb2z1aeS4burPflOLLS3TcHNS9gdctBKvxrNuN2O",
                    base64url(crypto.randomBytes(16).toString('hex')),
                    "1990-01-01",
                    "2016-07-25 17:13:02",
                    "2016-07-25 17:13:02",
                    r.id
                ];
            });
            var query=users.reduce(function(prev,user,index,users){
                return `${prev} ${(index===0?"":",")} ("${user[0]}","${user[1]}","${user[2]}","${user[3]}","${user[4]}","${user[5]}","${user[6]}",${user[7]})`;
            },'INSERT INTO users (`name`,`email`,`password`,`token`,`lastrun`,`created_at`,`updated_at`,`id`) VALUES');
            destination_connection.query(query, function (err, result) {
                if (err) throw err;
                r();
            });
        });
    });
}).then((users)=>{
    return p((r)=>{
        console.log("migrating homeoffice");
        source_connection.query('SELECT * from homeoffice', function (err, rows, fields) {
            if (err) throw err;
            
            var query=rows.reduce(function(prev,location,index,locations){
                return `${prev} ${(index===0?"":",")} ("${location.id}","${moment(location.date).format("YYYY-MM-DD")}","${location.location||location.plannedlocation}","${location.comments||""}",${location.user_id},"2016-07-25 17:13:02","2016-07-25 17:13:02")`;
            },'INSERT INTO pinned_locations (`id`, `date`, `location`, `description`, `user_id`, `created_at`, `updated_at`) VALUES');
            destination_connection.query(query, function (err, result) {
                if (err) throw err;
                r();
            });
        });
    });
}).then((users)=>{
    return p((r)=>{
        console.log("migrating holidays");
        source_connection.query("SELECT * FROM `calendar` WHERE Type='holiday'", function (err, rows, fields) {
            if (err) throw err;
            
            var query=rows.reduce(function(prev,holiday,index,holidays){
                return `${prev} ${(index===0?"":",")} ("${moment(holiday.Date).format("YYYY-MM-DD")}","${holiday.Description||""}","2016-07-25 17:13:02","2016-07-25 17:13:02")`;
            },'INSERT INTO holidays (`date`, `name`, `created_at`, `updated_at`) VALUES');
            destination_connection.query(query, function (err, result) {
                if (err) throw err;
                r();
            });
        });
    });
}).then(()=>{
    return p((r)=>{
        console.log("migrating roles");
        var validations=p((r)=>{
            destination_connection.query("INSERT INTO `role_user`(`user_id`, `role_id`) SELECT id, 2 from users", function (err, result) {
                if (err) throw err;
                r();
            });
        });
        var admin=p((r)=>{
            destination_connection.query("INSERT INTO `role_user`(`user_id`, `role_id`) VALUES (21,1)", function (err, result) {
                if (err) throw err;
                r();
            });
        });
        Promise.all([validations,admin]).then(()=>{
            r();
        });
    });
}).then(()=>{
    source_connection.end();
    destination_connection.end();
    console.log("success");
});